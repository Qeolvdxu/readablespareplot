function correct = readableSparsePlot(Q,name)

# Print standard sparse matrix list
disp("Sparse Matrix")
disp(Q)
disp("\n")

# Print Standard matrix table
disp("Full Matrix")
fullQ = full(Q);
disp(fullQ)
disp("\n")

#Get Matrix Sizes
ncols = size(fullQ,1);
nrows = size(fullQ,2);

# Build Plot Coord Arrays
y = [];
x = [];
for c = 1:ncols
  for r = 1:nrows
    if fullQ(c,r) != 0
      x(end+1) = r;
      y(end+1) = c;
    end
  end
end

# Construct Graph
Qscatter = scatter(x,(nrows+1)-y,100,'.m','filled');
grid on
title(name);
ylim([1 nrows])
xlim([1 ncols])
set(gca,'ytick',0:10)
set(gca,'xtick',0:10)
for i = 1:(ncols*nrows)
  val = fullQ(y(i),x(i));
  text(x(i),(nrows+1)-y(i),num2str(val))
end
waitfor(Qscatter)
  correct = true;
  
end
